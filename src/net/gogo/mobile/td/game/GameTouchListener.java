package net.gogo.mobile.td.game;

import java.util.ArrayList;

import net.gogo.mobile.framework.Vector3D;
import net.gogo.mobile.framework.game.GameObject2D;
import net.gogo.mobile.td.game.model.Trash;
import android.util.Log;
import android.view.MotionEvent;

public class GameTouchListener {
	private final static String TAG = "Game Touch Listener";
	public GameTouchListener(Game game) {
		mGame 		= game;
		mObjects 	= new ArrayList<ObjectPointerMapping>();
		mDeletedObjects	= new ArrayList<ObjectPointerMapping>();
	}
	
	public void onTouch(MotionEvent event) {
		if (mGame.isPaused()) return;
		
		final int action = event.getAction();
		final int actionCode = action & MotionEvent.ACTION_MASK;
		final int touchCount = event.getPointerCount();
		final int pointerInAction = action >> MotionEvent.ACTION_POINTER_ID_SHIFT;

		final float halfScreenHeight 	= GameGlobals.GAME_SCREEN_HEIGHT / 2;
		final float halfScreenWidth		= GameGlobals.GAME_SCREEN_WIDTH / 2;

		switch (actionCode) {
		// one pointer down
		case MotionEvent.ACTION_DOWN:
			int mappedX = (int)((event.getX()));
			int mappedY = (int)((event.getY() - halfScreenHeight + halfScreenWidth));
			
			// find any trash near this shit
			ArrayList<Trash> trashes = mGame.getTrashesOnPoint(mappedX, mappedY);

			for (int i = 0, n = trashes.size(); i < n; ++i) {
				final Trash trash = trashes.get(i);
				// save the range between the trash with finger
				mObjects.add(new ObjectPointerMapping(
						event.getPointerId(0),
						new Vector3D(trash.position.x - mappedX, trash.position.y - mappedY, 0),
						trash)
				);
				trash.bind();
			}
			
			break;
		// 1 more pointer is down
		case MotionEvent.ACTION_POINTER_DOWN:
			for (int i = 0; i < touchCount; ++i) {
				// for each touch, check whether this is the pointer in action
				if (event.getPointerId(i) != pointerInAction) continue;
				
				// when this is the pointer in action, add objects to affected pointer
				mappedX = (int)((event.getX(i)));
				mappedY = (int)((event.getY(i) - halfScreenHeight + halfScreenWidth));
				
				// find any trash near this shit
				trashes = mGame.getTrashesOnPoint(mappedX, mappedY);
				
				for (int j = 0, n = trashes.size(); j < n; ++j) {
					final Trash trash = trashes.get(j);
					// save the range between the trash with finger
					mObjects.add(new ObjectPointerMapping(
							event.getPointerId(i),
							new Vector3D(trash.position.x - mappedX, trash.position.y - mappedY, 0),
							trash)
					);
					trash.bind();
				}
			}
			break;
		// any of the pointers is up
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			// remove all object bound by this touch
			for (int i = 0; i < touchCount; ++i) {
				// for each touch, check whether this is the pointer in action
				if (event.getPointerId(i) != pointerInAction) continue;
				
				// remove all object bound by this touch
				for (int j = 0, n = mObjects.size(); j < n; ++j) {
					final ObjectPointerMapping obj = mObjects.get(j);
					
					if (obj.pointerID == pointerInAction) {
						mDeletedObjects.add(obj);
					}
				}
				
				// remove all object for sure!
				for (int j = 0, n = mDeletedObjects.size(); j < n; ++j) {
					final ObjectPointerMapping obj = mDeletedObjects.get(j);
					mObjects.remove(obj);
					
					// unbind the trash
					((Trash)obj.gameObject).unbind();
				}
				
				// clear the cache
				mDeletedObjects.clear();
			}
			break;
		// any of the pointer is moving
		case MotionEvent.ACTION_MOVE:
			for (int i = 0; i < touchCount; ++i) {
				// for each touch
				mappedX = (int)((event.getX(i)));
				mappedY = (int)((event.getY(i) - halfScreenHeight + halfScreenWidth));

				// if still moving in screen
				if (mappedX > 0 && mappedX < GameGlobals.GAME_SCREEN_WIDTH &&
						mappedY > 0 && mappedY < GameGlobals.GAME_SCREEN_WIDTH) {
					for (int j = 0, n = mObjects.size(); j < n; ++j) {
						final ObjectPointerMapping mapping = mObjects.get(j);
						final int pointerID = mapping.pointerID;
						final Vector3D vector = mapping.vector;
						final GameObject2D object = mapping.gameObject;
						
						// check whether this object handled by this touch
						if (pointerID != event.getPointerId(i)) continue;
						
						// if handled by the touch, then move the object
						object.position.x = mappedX + vector.x;
						object.position.y = mappedY + vector.y;
					}
				}
			}
			break;
		
		}
	}
	
	private class ObjectPointerMapping {
		public ObjectPointerMapping(int inPointerID, Vector3D inVector, GameObject2D inGameObject) {
			pointerID 	= inPointerID;
			vector 		= inVector;
			gameObject 	= inGameObject;
		}
		
		public int pointerID;
		public Vector3D vector;
		public GameObject2D gameObject;
	}
	
	private ArrayList<ObjectPointerMapping> mObjects;
	
	// deleted mapping
	private ArrayList<ObjectPointerMapping> mDeletedObjects;
	
	
	
	private Game mGame;
}
