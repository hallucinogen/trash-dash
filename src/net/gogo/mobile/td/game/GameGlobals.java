package net.gogo.mobile.td.game;

public class GameGlobals {

	public final static int GAME_FPS 			= 30;
	public final static int GAME_UPDATE_RATE 	= 1000 / GAME_FPS;
	
	// game screen resolution, may change when bound to activity
	public static int	GAME_SCREEN_WIDTH	= 320;
	public static int	GAME_SCREEN_HEIGHT	= 480;

	// game tile size, when bound will become the main measurement unit of the game
	public static int	GAME_TILE_WIDTH		= 32;
	public static int	GAME_TILE_HEIGHT	= 32;
}
