package net.gogo.mobile.td.game.level;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Point;
import android.util.Log;

import net.gogo.mobile.framework.game.GameObject2D;
import net.gogo.mobile.td.game.model.Bin;
import net.gogo.mobile.td.gl.MapRenderer;

public class Map extends GameObject2D {
	private final static String TAG = "Map";
	private Map() {
		super();
		
		// the level will always consist of 10x10 tile
		mSize.width 	= 10;
		mSize.height 	= 10;
		mTiles			= new int[10][10];
		mPaths 			= new ArrayList<ArrayList<Point>>();
	}

	public Map(String jsonString) {
		this();

		try {
			JSONObject json = new JSONObject(jsonString);

			mTileset = new Tileset(json.getString(JSON_KEY_TILESET));
			mGameDuration = json.getInt(JSON_KEY_TIME);

			// get all paths
			final JSONArray paths = json.getJSONArray(JSON_KEY_PATH);
			for (int i = 0, n = paths.length(); i < n; ++i) {
				// save each path
				final JSONArray path = paths.getJSONArray(i);
				final ArrayList<Point> realPath = new ArrayList<Point>();
				for (int j = 0, m = path.length(); j < m; j += 2) {
					realPath.add(new Point(path.getInt(j), path.getInt(j + 1)));
				}
				mPaths.add(realPath);
			}

			final JSONArray map = json.getJSONArray(JSON_KEY_TILE);
			int k = 0;
			for (int i = 0; i < 10; ++i) {
				for (int j = 0; j < 10; ++j) {
					// don't forget to subtract the tile with offset
					mTiles[i][j] = map.getInt(k) - mTileset.getOffset();
					++k;
				}
			}

			// make renderer
			mRenderer = new MapRenderer(mTileset, mTiles);

			// make the trash bin!
			mTrashBin = new Bin(json.getString(JSON_KEY_TRASH_BIN));
		} catch (JSONException jEx) {
			Log.i(TAG, "" + jEx);
		}	
	}
	
	@Override
	protected void drawRoutine(GL10 gl) {
		mRenderer.draw((GL11)gl);
		mTrashBin.draw(gl);
	}

	// getter
	public MapRenderer getRenderer() 				{	return mRenderer;			}
	public Bin getTrashBin() 					{	return mTrashBin;			}
	public int getGameDuration()					{	return mGameDuration;		}
	public int getPathsCount()						{	return mPaths.size();		}
	public ArrayList<Point> getPath(int index)		{	return mPaths.get(index);	}

	private MapRenderer mRenderer;
	private Bin mTrashBin;

	private final static String JSON_KEY_TILESET 	= "tileset";
	private final static String JSON_KEY_TILE 		= "map";
	private final static String JSON_KEY_PATH 		= "paths";
	private final static String JSON_KEY_TRASH_BIN 	= "bin";
	private final static String JSON_KEY_TIME		= "time";

	private ArrayList<ArrayList<Point>> mPaths;
	private int[][]	mTiles;
	private Tileset mTileset;
	private int mGameDuration;
}
