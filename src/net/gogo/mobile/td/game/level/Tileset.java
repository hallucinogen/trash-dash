package net.gogo.mobile.td.game.level;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import net.gogo.mobile.framework.glwrapper.GLTexture;
import net.gogo.mobile.framework.glwrapper.GLTiledTexture;
import net.gogo.mobile.td.gl.GLSystem;

public class Tileset {
	private final static String TAG = "Tileset";
	public Tileset(String jsonString) {
		try {
			JSONObject json = new JSONObject(jsonString);
			
			final String imageSource = json.getString(JSON_KEY_SRC);
			final int row = json.getInt(JSON_KEY_ROW);
			final int col = json.getInt(JSON_KEY_COL);
			
			// offset is the base number of tile. Offset value may vary between 0 or 1
			if (json.has(JSON_KEY_OFF)) {
				mOffset = json.getInt(JSON_KEY_OFF);
			} else {
				mOffset = 0;
			}
			
			mImages = new GLTiledTexture[row * col];

			GLTexture texture = GLSystem.GLTextureLibrary.allocateTexture(imageSource);

			for (int y = 0; y < row; ++y) {
				for (int x = 0; x < col; ++x) {
					final float top 	= (float)y / (float)row;
					final float left 	= (float)x / (float)col;
					final float bottom 	= (float)(y + 1) / (float)row;
					final float right 	= (float)(x + 1) / (float)col;
					
					mImages[y * col + x] = new GLTiledTexture(texture, 
							new float[] {
								left, bottom,
								right, bottom,
								left, top,
								right, top
							}
					);
				}
			}
		} catch (JSONException jEx) {
		}
	}
	
	public GLTiledTexture getTile(int index) {
		return mImages[index];
	}
	
	// offset is the base number of tile. Offset value may vary between 0 or 1
	public int getOffset() {
		return mOffset;
	}
	
	private GLTiledTexture[] mImages;
	
	// offset is the base number of tile. Offset value may vary between 0 or 1
	private int mOffset;
	
	private final static String JSON_KEY_SRC = "src";
	private final static String JSON_KEY_ROW = "row";
	private final static String JSON_KEY_COL = "col";
	private final static String JSON_KEY_OFF = "offset";
}
