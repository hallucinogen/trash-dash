package net.gogo.mobile.td.game.model;

import net.gogo.mobile.framework.Dimension;
import net.gogo.mobile.framework.glwrapper.GLSprite;
import net.gogo.mobile.td.game.GameGlobals;
import net.gogo.mobile.td.gl.GLSystem;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class VisitorData {
	private final static String TAG = VisitorData.class.getSimpleName();
	public VisitorData(String jsonString) {
		try {
			// decode the json
			JSONObject json = new JSONObject(jsonString);
			
			mSprite 	= new GLSprite(json.getString(JSON_KEY_SPRITE), GLSystem.GLTextureLibrary);
			mSpeed 		= (float)json.getDouble(JSON_KEY_SPEED);
			mSize		= new Dimension((float)json.getDouble(JSON_KEY_WIDTH) * GameGlobals.GAME_TILE_WIDTH,
						(float)json.getDouble(JSON_KEY_HEIGHT)  * GameGlobals.GAME_TILE_HEIGHT);
			mName		= json.getString(JSON_KEY_NAME);
			mStayTime	= json.getInt(JSON_KEY_STAY_TIME);
			mStayProb 	= json.getInt(JSON_KEY_STAY_PROB);
		} catch (JSONException jEx) {
			Log.i(TAG, jEx.toString());
		}
	}
	
	public float getWidth() 		{	return mSize.width;		}
	public float getHeight()		{	return mSize.height;	}
	public float getSpeed()			{	return mSpeed;			}
	public int getStayTime()		{	return mStayTime;		}
	public int getStayProbability()	{	return mStayProb;		}
	public GLSprite getSprite()		{	return mSprite;			}
	public String getName()			{	return mName;			}
	
	private Dimension mSize;
	private GLSprite mSprite;
	
	// walking move speed
	private float mSpeed;
	
	// stay time is the time spent when this guy decide to stay
	private int mStayTime;
	
	// stay chance is the chance this guy decide to stay. Value falls in 0..9
	private int mStayProb;
	private String mName;
	
	private final static String JSON_KEY_STAY_TIME 	= "stayTime";
	private final static String JSON_KEY_STAY_PROB	= "stayProb";
	private final static String JSON_KEY_SPEED 		= "speed";
	private final static String JSON_KEY_WIDTH 		= "width";
	private final static String JSON_KEY_HEIGHT		= "height";
	private final static String JSON_KEY_SPRITE 	= "sprite";
	private final static String JSON_KEY_NAME		= "name";
}
