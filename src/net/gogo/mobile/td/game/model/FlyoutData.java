package net.gogo.mobile.td.game.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import net.gogo.mobile.framework.Dimension;
import net.gogo.mobile.framework.glwrapper.GLSprite;
import net.gogo.mobile.td.game.GameGlobals;
import net.gogo.mobile.td.gl.GLSystem;

public class FlyoutData {
	private final static String TAG = FlyoutData.class.getSimpleName();
	public FlyoutData(String jsonString) {
		try {
			// decode the json
			JSONObject json = new JSONObject(jsonString);

			mSprite 	= new GLSprite(json.getString(JSON_KEY_SPRITE), GLSystem.GLTextureLibrary);
			mDuration 	= json.getInt(JSON_KEY_DURATION);
			mSize		= new Dimension((float)json.getDouble(JSON_KEY_WIDTH) * GameGlobals.GAME_TILE_WIDTH,
						(float)json.getDouble(JSON_KEY_HEIGHT)  * GameGlobals.GAME_TILE_HEIGHT);
			mName		= json.getString(JSON_KEY_NAME);
			mFollowing 	= json.has(JSON_KEY_FOLLOWING) ? json.getBoolean(JSON_KEY_FOLLOWING) : false;
		} catch (JSONException jEx) {
			Log.e(TAG, jEx.toString());
		}
	}

	public float getWidth() 		{	return mSize.width;		}
	public float getHeight()		{	return mSize.height;	}
	public int getDuration()		{	return mDuration;		}
	public GLSprite getSprite()		{	return mSprite;			}
	public String getName()			{	return mName;			}
	public boolean isFollowing()	{	return mFollowing;		}

	private String mName;

	private Dimension mSize;
	private GLSprite mSprite;
	private int mDuration;

	/** determine whether the flyout is following an object or not */
	private boolean mFollowing;

	private final static String JSON_KEY_WIDTH 		= "width";
	private final static String JSON_KEY_HEIGHT		= "height";
	private final static String JSON_KEY_SPRITE 	= "sprite";
	private final static String JSON_KEY_NAME		= "name";
	private final static String JSON_KEY_DURATION	= "duration";
	private final static String JSON_KEY_FOLLOWING	= "following";
}
