package net.gogo.mobile.td.game.model;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Point;
import android.util.Log;

import net.gogo.mobile.framework.Vertex2D;
import net.gogo.mobile.framework.game.GameObject2D;
import net.gogo.mobile.framework.glwrapper.GLSprite;
import net.gogo.mobile.framework.utilities.DrawUtilities;
import net.gogo.mobile.framework.utilities.GameRandom;
import net.gogo.mobile.td.game.GameGlobals;

public class Visitor extends GameObject2D {
	private final static String TAG = Visitor.class.getSimpleName();
	public Visitor(GLSprite sprite, float width, float height) {
		super(sprite, width, height);
		mIndexInPath 	= 1;
		mIsWaiting 		= false;
		mAlive			= true;
	}

	public Visitor(VisitorData data) {
		this(data.getSprite(), data.getWidth(), data.getHeight());
		// persist data
		mData = data;
		// make randomized speed
		mSpeed = mData.getSpeed() + mData.getSpeed() * GameRandom.floatRandom() / 3.0f;
	}

	@Override
	public void update(long time) {
		// if not alive, don't update!
		if (!mAlive) return;

		// check whether he's waiting or not
		if (mIsWaiting) {
			mWaitTime += time;
			// if he should still be waiting, then do nothing
			//Log.e(TAG, "wait time = " + mWaitTime + " stay time = " + mData.getStayTime() + " index in path = " + mIndexInPath);
			if (mWaitTime < mData.getStayTime()) return;
		} else {
			// ok he's moving
			
			// update sprite!
			mSprite.update(time);
			
			// get the center of this guy
			final float centerX = position.x + mSize.width / 2;
			final float centerY = position.y + mSize.height / 2;

			final float targetCenterX = mPath.get(mIndexInPath).x * GameGlobals.GAME_TILE_WIDTH + GameGlobals.GAME_TILE_WIDTH / 2;
			final float targetCenterY = mPath.get(mIndexInPath).y * GameGlobals.GAME_TILE_HEIGHT + GameGlobals.GAME_TILE_HEIGHT / 2;
			
			// check direction
			final int direction = getDirectionByCurrentPath();
			final int dirX = MOVE_X[direction];
			final int dirY = MOVE_Y[direction];
			
			//Log.e(TAG, "center = " + centerX + ", " + centerY + " targetCenter = " + targetCenterX + ", " + targetCenterY);
			
			// if the range between target and current is super duper small!
			if (Vertex2D.range(centerX, centerY, targetCenterX, targetCenterY) < mSpeed * time) {
				// align to right position
				position.x = targetCenterX - mSize.width / 2;
				position.y = targetCenterY - mSize.height / 2;
			} else {
				// he's moving normally
				position.x += dirX * (float)mSpeed * time;
				position.y += dirY * (float)mSpeed * time;
				return;
			}
		}

		// is going to move. First let's check whether he's really trying to move or not
		final int moveProbability = GameRandom.unsignedIntRandom(9);

		if (moveProbability <= mData.getStayProbability()) {
			// going to stay in place!
			mWaitTime = 0;
			mIsWaiting = true;

			mSprite.playAnimation(getDirectionByCurrentPath(), false, false);
		} else {
			mIsWaiting = false;
			// this guy is going to leave it's current grid
			++mIndexInPath;
			if (mIndexInPath == mPath.size()) {
				// player is reaching its end path, thus die
				mAlive = false;
			} else {
				mSprite.playAnimation(getDirectionByCurrentPath(), true, false);
			}
		}
	}

	@Override
	protected void drawRoutine(GL10 gl) {
		// if not alive, don't draw!
		if (!mAlive) return;
		
		DrawUtilities.instance().draw(gl, mSprite, position.x, position.y, mSize.width, mSize.height, -1.01f);
	}

	public VisitorData getData()				{	return mData;		}
	public ArrayList<Point> getPath()			{	return mPath;		}
	public boolean isAlive()					{	return mAlive;		}
	public void setAlive(boolean alive)			{	mAlive = alive;		}
	

	public void setPath(ArrayList<Point> path)	{	
		mPath = path;
		
		// reset the position
		mIndexInPath = 1;
		position.x = mPath.get(0).x * GameGlobals.GAME_TILE_WIDTH + GameGlobals.GAME_TILE_WIDTH / 2 - mSize.width / 2;
		position.y = mPath.get(0).y * GameGlobals.GAME_TILE_HEIGHT + GameGlobals.GAME_TILE_HEIGHT / 2 - mSize.height / 2;

		// start animation
		mSprite.playAnimation(getDirectionByCurrentPath(), true, false);
	}
	
	private int getDirectionByCurrentPath() {
		final int dx = mPath.get(mIndexInPath).x - mPath.get(mIndexInPath - 1).x;
		final int dy = mPath.get(mIndexInPath).y - mPath.get(mIndexInPath - 1).y;
		
		if (dx > 0) return 0;
		if (dx < 0) return 1;
		if (dy > 0) return 2;
		return 3;
	}
	
	// visitor data
	private VisitorData mData;
	private float mSpeed;
	
	// living stuff
	private boolean mAlive;
	
	// path for moving
	private ArrayList<Point> mPath;
	private int mIndexInPath;
	
	// wait stuff
	private long mWaitTime;
	private boolean mIsWaiting;
	

	// move stuff
	private static int[] MOVE_X = {1, -1, 0, 0};
	private static int[] MOVE_Y = {0, 0, 1, -1};
}
