package net.gogo.mobile.td.game.model;

import javax.microedition.khronos.opengles.GL10;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import net.gogo.mobile.framework.Dimension;
import net.gogo.mobile.framework.game.GameObject2D;
import net.gogo.mobile.framework.glwrapper.GLSprite;
import net.gogo.mobile.framework.utilities.DrawUtilities;
import net.gogo.mobile.td.game.GameGlobals;
import net.gogo.mobile.td.gl.GLSystem;

public class Bin extends GameObject2D {
	private final static String TAG = "Trash Bin";
	public Bin(GLSprite sprite, float width, float height) {
		super(sprite, width, height);
	}
	
	public Bin(String jsonString) {
		super();
		try {
			JSONObject json = new JSONObject(jsonString);
			
			// make sprite definition
			mSprite = new GLSprite(json.getString(JSON_KEY_SPRITE), GLSystem.GLTextureLibrary);
			
			// get position definition
			final int tileX = json.getInt(JSON_KEY_X);
			final int tileY = json.getInt(JSON_KEY_Y);
			
			position.x = tileX * GameGlobals.GAME_TILE_WIDTH /*- GameGlobals.GAME_SCREEN_WIDTH / 2*/;
			position.y = tileY * GameGlobals.GAME_TILE_HEIGHT /*- GameGlobals.GAME_SCREEN_WIDTH / 2*/;
			//Log.e(TAG, "GameGlobals dimension = " + GameGlobals.GAME_TILE_WIDTH + " " + GameGlobals.GAME_SCREEN_WIDTH + " " + GameGlobals.GAME_SCREEN_HEIGHT);
			Log.e(TAG, "Trash position = " + position + " " + tileY);
			
			initialize(mSprite, GameGlobals.GAME_TILE_WIDTH, GameGlobals.GAME_TILE_HEIGHT);
		} catch (JSONException jEx) {
			Log.i(TAG, jEx.toString());
		}
	}
	
	@Override
	public void update(long time) {
		// update sprite
		if (mSprite != null) {
			mSprite.update(time);
		}
	}
	
	@Override
	protected void drawRoutine(GL10 gl) {
		DrawUtilities.instance().draw(gl, mSprite, position.x, position.y, mSize.width, mSize.height, -1.015f);
	}
	
	private final static String JSON_KEY_SPRITE = "sprite";
	private final static String JSON_KEY_X 		= "x";
	private final static String JSON_KEY_Y 		= "y";
}
