package net.gogo.mobile.td.game.model;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import net.gogo.mobile.framework.utilities.FileUtilities;

public class TrashFactory {
	private final static String TAG = TrashFactory.class.getSimpleName();
	private TrashFactory() {
		mDataMap = new HashMap<String, TrashData>();
	}
	
	private TrashFactory(Context context) {
		this();
		loadResourceByContext(context);
	}
	
	private void loadResourceByContext(Context context) {
		// try decode trash data
		try {
			String trashJSON = FileUtilities.readFileAsString(context.getAssets().open("data/trashes.json"));
			JSONArray jsonArray = new JSONArray(new JSONObject(trashJSON).getString("trashses"));
			
			for (int i = 0, n = jsonArray.length(); i < n; ++i) {
				TrashData data = new TrashData(jsonArray.getString(i));
				mDataMap.put(data.getName(), data);
			}
		} catch (JSONException jEx) {
			Log.i(TAG, jEx.toString());
		} catch (IOException iEx) {
			Log.i(TAG, iEx.toString());
		}
	}

	public static TrashFactory instance() {
		if (sInstance == null) {
			sInstance = new TrashFactory();
		}

		return sInstance;
	}
	
	public static TrashFactory instance(Context context) {
		if (sInstance == null) {
			sInstance = new TrashFactory();
		}
		
		sInstance.loadResourceByContext(context);
		
		return sInstance;
	}
	
	public TrashData getData(String dataName) {
		return mDataMap.get(dataName);
	}
	
	public TrashData getData(int dataID) {
		return mDataMap.get(TRASH_NAME[dataID]);
	}
	
	// data map
	private HashMap<String, TrashData> mDataMap;
	
	private static final String[] TRASH_NAME = {"paper", "can"};
	
	private static TrashFactory sInstance;
}
