package net.gogo.mobile.td.game.model;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import net.gogo.mobile.framework.utilities.FileUtilities;

public class VisitorFactory {
	private final static String TAG = VisitorFactory.class.getSimpleName();
	private VisitorFactory() {
		mDataMap = new HashMap<String, VisitorData>();
	}

	private VisitorFactory(Context context) {
		this();
		loadResourceByContext(context);
	}
	
	private void loadResourceByContext(Context context) {
		// try decode trash data
		try {
			String trashJSON = FileUtilities.readFileAsString(context.getAssets().open("data/visitor.json"));
			JSONArray jsonArray = new JSONArray(new JSONObject(trashJSON).getString("visitor"));
			
			for (int i = 0, n = jsonArray.length(); i < n; ++i) {
				final VisitorData data = new VisitorData(jsonArray.getString(i));
				mDataMap.put(data.getName(), data);
			}
		} catch (JSONException jEx) {
			Log.i(TAG, jEx.toString());
		} catch (IOException iEx) {
			Log.i(TAG, iEx.toString());
		}
	}

	public static VisitorFactory instance() {
		if (sInstance == null) {
			sInstance = new VisitorFactory();
		}
		
		return sInstance;
	}
	
	public static VisitorFactory instance(Context context) {
		if (sInstance == null) {
			sInstance = new VisitorFactory();
		}
		
		sInstance.loadResourceByContext(context);
		
		return sInstance;
	}
	
	public VisitorData getData(String dataName) {
		return mDataMap.get(dataName);
	}
	
	public VisitorData getData(int dataID) {
		return mDataMap.get(PEOPLE_NAME[dataID]);
	}
	
	// data map
	private HashMap<String, VisitorData> mDataMap;
	
	private static final String[] PEOPLE_NAME = {"girl", "boy", "mib"};
	
	private static VisitorFactory sInstance;
}
