package net.gogo.mobile.td.game.model;

import java.io.IOException;

import javax.microedition.khronos.opengles.GL10;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import net.gogo.mobile.framework.Dimension;
import net.gogo.mobile.framework.game.GameObject2D;
import net.gogo.mobile.framework.glwrapper.GLSprite;
import net.gogo.mobile.framework.utilities.DrawUtilities;
import net.gogo.mobile.framework.utilities.FileUtilities;
import net.gogo.mobile.td.game.GameGlobals;
import net.gogo.mobile.td.gl.GLSystem;

public class HygieneBar extends GameObject2D {
	private final static String TAG = HygieneBar.class.getSimpleName();

	{
		mHygiene = 100;
	}

	public HygieneBar(Context context) {
		try {
			String hygieneJSON = FileUtilities.readFileAsString(context.getAssets().open("data/hygiene_bar.json"));
			JSONObject json = new JSONObject(hygieneJSON);

			mSprite = new GLSprite(json.getString(JSON_KEY_SPRITE), GLSystem.GLTextureLibrary);
			mSize = new Dimension(GameGlobals.GAME_SCREEN_WIDTH, GameGlobals.GAME_SCREEN_WIDTH / 256 * 16);
		} catch (JSONException jEx) {
			Log.i(TAG, jEx.toString());
		} catch (IOException iEx) {
			Log.i(TAG, iEx.toString());
		}
	}
	
	public void addHygiene(int hygiene) {
		mHygiene += hygiene;
		
		// make sure value stays in range
		if (mHygiene > MAX_HYGIENE_VALUE) {
			mHygiene = MAX_HYGIENE_VALUE;
		} else if (mHygiene < MIN_HYGIENE_VALUE) {
			mHygiene = MIN_HYGIENE_VALUE;
		}

		final float hygienePercentage = (float)(mHygiene - MIN_HYGIENE_VALUE) / (float)(MAX_HYGIENE_VALUE - MIN_HYGIENE_VALUE);
		mSprite.getImage().crop[2] = hygienePercentage;
		mSprite.getImage().crop[6] = hygienePercentage;
		mSize.width = GameGlobals.GAME_SCREEN_WIDTH * hygienePercentage;
	}
	
	@Override
	protected void drawRoutine(GL10 gl) {
		DrawUtilities.instance().draw(gl, mSprite, position.x, position.y, mSize.width, mSize.height, -1.01f);
	}

	/** hygiene value. Value falls in 0-100 */
	private int mHygiene;
	
	private final static int MIN_HYGIENE_VALUE = 0;
	private final static int MAX_HYGIENE_VALUE = 100;
	
	/** JSON key stuffs */
	private final static String JSON_KEY_SPRITE = "sprite";
}
