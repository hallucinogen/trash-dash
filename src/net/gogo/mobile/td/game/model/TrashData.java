package net.gogo.mobile.td.game.model;

import net.gogo.mobile.framework.Dimension;
import net.gogo.mobile.framework.glwrapper.GLSprite;
import net.gogo.mobile.td.game.GameGlobals;
import net.gogo.mobile.td.gl.GLSystem;

import org.json.JSONException;
import org.json.JSONObject;

import android.R.dimen;
import android.util.Log;

public class TrashData {
	private final static String TAG = TrashData.class.getSimpleName();
	public TrashData(String jsonString) {
		try {
			// decode the json
			JSONObject json = new JSONObject(jsonString);

			mSprite 	= new GLSprite(json.getString(JSON_KEY_SPRITE), GLSystem.GLTextureLibrary);
			mScore 		= json.getInt(JSON_KEY_SCORE);
			mDecay 		= json.getInt(JSON_KEY_DECAY_VALUE);
			mRate 		= json.getInt(JSON_KEY_DECAY_RATE);
			mDuration 	= json.getInt(JSON_KEY_DECAY_DURATION);
			mGreater 	= json.has(JSON_KEY_DECAY_GREATER) ? json.getBoolean(JSON_KEY_DECAY_GREATER) : false;
			mSize		= new Dimension((float)json.getDouble(JSON_KEY_WIDTH) * GameGlobals.GAME_TILE_WIDTH,
						(float)json.getDouble(JSON_KEY_HEIGHT)  * GameGlobals.GAME_TILE_HEIGHT);
			mName		= json.getString(JSON_KEY_NAME);
		} catch (JSONException jEx) {
			Log.e(TAG, jEx.toString());
		}
	}
	
	public float getWidth() 	{	return mSize.width;		}
	public float getHeight()	{	return mSize.height;	}
	public int getScore()		{	return mScore;			}
	public int getDecay()		{	return mDecay;			}
	public int getRate()		{	return mRate;			}
	public int getDuration()	{	return mDuration;		}
	public GLSprite getSprite()	{	return mSprite;			}
	public String getName()		{	return mName;			}
	public boolean isGreater()	{	return mGreater;		}
	
	private Dimension mSize;
	private GLSprite mSprite;
	private int mScore, mDecay, mRate, mDuration;
	private String mName;

	/** if decay is greater, trash decay value will increase overtime */
	private boolean mGreater;
	
	private final static String JSON_KEY_SCORE 			= "score";
	private final static String JSON_KEY_WIDTH 			= "width";
	private final static String JSON_KEY_HEIGHT			= "height";
	private final static String JSON_KEY_SPRITE 		= "sprite";
	private final static String JSON_KEY_NAME			= "name";
	private final static String JSON_KEY_DECAY_VALUE	= "decay";
	private final static String JSON_KEY_DECAY_RATE		= "rate";
	private final static String JSON_KEY_DECAY_DURATION	= "duration";
	private final static String JSON_KEY_DECAY_GREATER	= "isGreater";
}
