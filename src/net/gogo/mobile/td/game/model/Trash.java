package net.gogo.mobile.td.game.model;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Point;
import android.util.Log;

import net.gogo.mobile.framework.Dimension;
import net.gogo.mobile.framework.Vector3D;
import net.gogo.mobile.framework.Vertex2D;
import net.gogo.mobile.framework.game.GameObject2D;
import net.gogo.mobile.framework.glwrapper.GLSprite;
import net.gogo.mobile.framework.utilities.DrawUtilities;

public class Trash extends GameObject2D {
	private final static String TAG = Trash.class.getSimpleName();

	public interface TrashStateListener {
		public void onTrashDamage(Trash trash);
	}

	public Trash(GLSprite sprite, float width, float height) {
		super(sprite, width, height);
		mLifetime 	= 0;
		mAlive 		= true;
		mDidDamage	= 0;
	}

	public Trash(TrashData data, float rotation) {
		this(data.getSprite(), data.getWidth(), data.getHeight());
		// persist data
		mRotation 	= rotation;
		mData		= data;
	}

	@Override
	public void update(long time) {
		if (!mAlive) {
			// remove listener
			mListener = null;
			return;
		}

		mLifetime += time;

		switch (mState) {
		case TRASH_THROWN:
			// over 1 sec. Let's stop the trash
			if (mLifetime > 1000) {
				mState 		= TRASH_STOP;
				mLifetime 	= 0;
			} else {
				position.x += mThrowVelocity.x * time;
				position.y += mThrowVelocity.y * time;
			}
			break;

		case TRASH_BOUND:
			
			break;
		case TRASH_STOP:
			if (mLifetime > mData.getDuration()) {
				mAlive = false;
			}
	
			if (mLifetime >= (mDidDamage + 1) * mData.getRate()) {
				++mDidDamage;
				if (mListener != null) {
					mListener.onTrashDamage(this);
				}
			}

			break;
		}
	}

	@Override
	protected void drawRoutine(GL10 gl) {
		DrawUtilities.instance().draw(gl, mSprite, position.x, position.y, mSize.width, mSize.height, -1.01f, mRotation);
	}

	public void throwTrash(Vertex2D from, Vertex2D to) {
		position.x = from.x;
		position.y = from.y;

		// init object
		if (mThrowVelocity == null) 		mThrowVelocity = new Vector3D();

		// horizontal velocity
		mThrowVelocity.x = to.x - from.x;
		mThrowVelocity.y = to.y - from.y;

		// set to 1/1000 sec order
		mThrowVelocity.x /= 1000.0f;
		mThrowVelocity.y /= 1000.0f;

		mState = TRASH_THROWN;
	}

	public TrashData getData()				{	return mData;					}
	public boolean isBound() 				{	return mState == TRASH_BOUND;	}
	public boolean isAlive()				{	return mAlive;					}
	public float getRotation()				{	return mRotation;				}
	/** Bind a trash to user finger to let everyone know that this trash is bound */
	public void bind()						{	mState = TRASH_BOUND;			}
	/** Unbind trash from user finger to let everyone know that this trash is released */
	public void unbind()					{	mState = TRASH_STOP;			}
	
	public void setTrashListener(TrashStateListener listener) {
		mListener = listener;
	}

	// if the trash bounded, it's still in player's finger and shall not drop to trash bin
	private int mState;

	// trash data
	private TrashData mData;
	private float mRotation;

	// lifetime stuff
	private long mLifetime;
	private boolean mAlive;
	private int mDidDamage;

	// throwing stuff
	private Vector3D mThrowVelocity;

	// listener
	private TrashStateListener mListener;

	public final static int TRASH_THROWN 	= 0;
	public final static int TRASH_STOP 		= 1;
	public final static int TRASH_BOUND 	= 2;
}
