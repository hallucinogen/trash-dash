package net.gogo.mobile.td.game.model;

import javax.microedition.khronos.opengles.GL10;

import net.gogo.mobile.framework.Vector3D;
import net.gogo.mobile.framework.Vertex3D;
import net.gogo.mobile.framework.game.GameObject;
import net.gogo.mobile.framework.game.GameObject2D;
import net.gogo.mobile.framework.utilities.DrawUtilities;
import net.gogo.mobile.td.game.GameGlobals;

public class Flyout extends GameObject2D {
	{
		mAlive 		= true;
		mLifetime 	= 0;
		mDistanceFromMainObject = new Vector3D();
	}

	public Flyout(FlyoutData data, Vertex3D inPosition) {
		super(data.getSprite(), data.getWidth(), data.getHeight());
		// persist data
		mData = data;
		mBaseObjectPosition = inPosition;
	}

	@Override
	public void update(long time) {
		if (!mAlive) {
			// remove followed object
			mBaseObjectPosition = null;
			return;
		}

		mLifetime += time;

		if (mLifetime > mData.getDuration()) {
			mAlive = false;
		} else {
			// update flyout position
			position.x = mBaseObjectPosition.x;
			position.y = mBaseObjectPosition.y;
			if (mData.isFollowing()) {
				mDistanceFromMainObject.y = -GameGlobals.GAME_TILE_HEIGHT * COEF_FOLLOWING_FLYOUT_DISTANCE;
			} else {
				mDistanceFromMainObject.y -= time * COEF_FLYOUT_VERTICAL_SPEED * GameGlobals.GAME_TILE_HEIGHT;
			}

			// vibrate the flyout :)
			mDistanceFromMainObject.x = (float)Math.sin(Math.toRadians(mLifetime * COEF_FLYOUT_VIBRATION_RATE)) * GameGlobals.GAME_TILE_WIDTH * COEF_FLYOUT_VIBRATION_AMPLITUDE;
		
			position.x += mDistanceFromMainObject.x;
			position.y += mDistanceFromMainObject.y;
		}
	}

	@Override
	protected void drawRoutine(GL10 gl) {
		DrawUtilities.instance().draw(gl, mSprite, position.x, position.y, mSize.width, mSize.height, -1.01f);
	}

	public FlyoutData getData()				{	return mData;					}
	public boolean isAlive()				{	return mAlive;					}

	private FlyoutData mData;

	/** followed object by flyout */
	private Vertex3D mBaseObjectPosition;

	/** distance from main object (either following or not) */
	private Vector3D mDistanceFromMainObject;

	// lifetime stuff
	private long mLifetime;
	private boolean mAlive;

	private final static float COEF_FOLLOWING_FLYOUT_DISTANCE 	= 0.3f;
	private final static float COEF_FLYOUT_VERTICAL_SPEED 		= 0.0005f;
	private final static float COEF_FLYOUT_VIBRATION_RATE 		= 0.5f;
	private final static float COEF_FLYOUT_VIBRATION_AMPLITUDE 	= 0.04f;
}
