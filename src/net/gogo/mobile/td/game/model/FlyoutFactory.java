package net.gogo.mobile.td.game.model;

import java.io.IOException;
import java.util.HashMap;

import net.gogo.mobile.framework.utilities.FileUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class FlyoutFactory {
	private final static String TAG = FlyoutFactory.class.getSimpleName();

	private FlyoutFactory() {
		mDataMap = new HashMap<String, FlyoutData>();
	}
	
	private FlyoutFactory(Context context) {
		this();
		loadResourceByContext(context);
	}
	
	private void loadResourceByContext(Context context) {
		// try decode trash data
		try {
			String trashJSON = FileUtilities.readFileAsString(context.getAssets().open("data/flyouts.json"));
			JSONArray jsonArray = new JSONArray(new JSONObject(trashJSON).getString("flyouts"));
			
			for (int i = 0, n = jsonArray.length(); i < n; ++i) {
				FlyoutData data = new FlyoutData(jsonArray.getString(i));
				mDataMap.put(data.getName(), data);
			}
		} catch (JSONException jEx) {
			Log.i(TAG, jEx.toString());
		} catch (IOException iEx) {
			Log.i(TAG, iEx.toString());
		}
	}

	public static FlyoutFactory instance() {
		if (sInstance == null) {
			sInstance = new FlyoutFactory();
		}

		return sInstance;
	}
	
	public static FlyoutFactory instance(Context context) {
		if (sInstance == null) {
			sInstance = new FlyoutFactory();
		}
		
		sInstance.loadResourceByContext(context);
		
		return sInstance;
	}

	public FlyoutData getData(String dataName) {
		return mDataMap.get(dataName);
	}
	
	public FlyoutData getData(int dataID) {
		return mDataMap.get(FLYOUT_NAME[dataID]);
	}

	// data map
	private HashMap<String, FlyoutData> mDataMap;

	private static final String[] FLYOUT_NAME = {"money", "love"};

	private static FlyoutFactory sInstance;
}
