package net.gogo.mobile.td.game;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Point;
import android.util.Log;

import net.gogo.mobile.framework.Vertex2D;
import net.gogo.mobile.framework.Vertex3D;
import net.gogo.mobile.framework.utilities.GameRandom;
import net.gogo.mobile.td.game.level.Map;
import net.gogo.mobile.td.game.model.Flyout;
import net.gogo.mobile.td.game.model.FlyoutFactory;
import net.gogo.mobile.td.game.model.HygieneBar;
import net.gogo.mobile.td.game.model.Visitor;
import net.gogo.mobile.td.game.model.VisitorFactory;
import net.gogo.mobile.td.game.model.Trash;
import net.gogo.mobile.td.game.model.Bin;
import net.gogo.mobile.td.game.model.TrashFactory;
import net.gogo.mobile.td.gl.GLSystem;
import net.gogo.mobile.td.gl.MapRenderer;

public class Game extends Thread implements Trash.TrashStateListener {
	private final static String TAG = "Game";
	public Game(String mapJSONString) {
		mTrashes 		= new ArrayList<Trash>();
		mVisitor		= new ArrayList<Visitor>();
		mFlyouts		= new ArrayList<Flyout>();

		mCacheTrashes	= new ArrayList<Trash>();
		mCacheVisitor	= new ArrayList<Visitor>();
		mCacheFlyouts	= new ArrayList<Flyout>();

		mDeletedTrashes	= new ArrayList<Trash>();
		mDeletedVisitor	= new ArrayList<Visitor>();
		mDeletedFlyouts = new ArrayList<Flyout>();

		// cache vertex for computation
		mCacheVertex1 = new Vertex3D();
		mCacheVertex2 = new Vertex3D();

		// save the json map for resize!
		mMapJSONString = mapJSONString;

		// reset game duration
		mGameDuration = 0;
		mEntitiesReady = false;

		// reset score
		mScore = 0;
		debugMode();
	}

	private void debugMode() {
		mSpawnTime = 5000;
		mGameTime = 0;
		mTrashVariationEachSpawn = 1;
	}
	
	private void makeEntities() {
		mTrashes.clear();
		mVisitor.clear();

		// make the map
		mMap = new Map(mMapJSONString);
		
		// reset timer!
		mGameDuration = mMap.getGameDuration() * 1000;
		
		// get the entities from map
		mTrashBin = mMap.getTrashBin();

		mEntitiesReady = true;
	}

	// resize entities to fit the screen
	public void resizeEntities() {
		// TODO: remake this! This is too costly!
		makeEntities();
	}

	@Override
	public void run() {
		mRunning = true;
		
		while (mRunning) {
			long difference = 0;
			long current = System.currentTimeMillis();
			
			if (difference < GameGlobals.GAME_UPDATE_RATE) {
				try {
					Thread.sleep(GameGlobals.GAME_UPDATE_RATE - difference);
				} catch (InterruptedException ex) {}
			}

			//Save current time
			difference 	= System.currentTimeMillis() - current;
			current 	= System.currentTimeMillis();

			if (!mPaused) {
				synchronized (GLSystem.GameRenderer) {
					update(difference);
				}
			}
		}
	}

	private void update(long time) {
		if (!mEntitiesReady) return;

		// TODO: handle pause
		mGameDuration -= time;
		if (mTimeChangedListener != null) {
			mTimeChangedListener.onTimeChange(mGameDuration);
		}

		// game ended, let's end the game
		if (mGameDuration <= 0) {
			mPaused = true;
			// TODO: do something victory-ish
			return;
		}
		
		checkTrashes(time);
		checkVisitor(time);
		checkFlyouts(time);
	}

	private void checkTrashes(long time) {
		// check if there's any trash on the trash bin
		for (int i = 0, n = mTrashes.size(); i < n; ++i) {
			final Trash trash = mTrashes.get(i);
			if (!trash.isBound() && Vertex2D.range(trash.position, mTrashBin.position) < GameGlobals.GAME_TILE_WIDTH) {
				// trash enter the trash bin, let's add the score!
				mDeletedTrashes.add(trash);
				mScore += trash.getData().getScore();
				if (mScoreChangedListener != null) {
					mScoreChangedListener.onScoreChange(mScore);
				}
				// since trash enter trash bin, let's add hygienity
				mHygieneBar.addHygiene(trash.getData().getDecay() * 2);
			} else if (!trash.isAlive()) {
				mDeletedTrashes.add(trash);
			} else {
				trash.update(time);
			}
		}
		
		for (int i = 0, n = mDeletedTrashes.size(); i < n; ++i) {
			final Trash trash = mDeletedTrashes.get(i);
			mTrashes.remove(trash);
		}
		mDeletedTrashes.clear();

		debugSpawnTrash(time);
	}

	private void checkVisitor(long time) {
		for (int i = 0, n = mVisitor.size(); i < n; ++i) {
			final Visitor visitor = mVisitor.get(i);
			if (!visitor.isAlive()) {
				mDeletedVisitor.add(visitor);

				// TODO: this is dummy
				// spawn money
				final Flyout money = new Flyout(FlyoutFactory.instance().getData("money"), visitor.position);
				mFlyouts.add(money);
			} else {
				visitor.update(time);
			}
		}

		for (int i = 0, n = mDeletedVisitor.size(); i < n; ++i) {
			final Visitor visitor = mDeletedVisitor.get(i);
			mVisitor.remove(visitor);
		}
		mDeletedVisitor.clear();

		debugSpawnVisitor(time);
	}

	private void checkFlyouts(long time) {
		for (int i = 0, n = mFlyouts.size(); i < n; ++i) {
			final Flyout flyout = mFlyouts.get(i);
			if (!flyout.isAlive()) {
				mDeletedFlyouts.add(flyout);
			} else {
				flyout.update(time);
			}
		}

		for (int i = 0, n = mDeletedFlyouts.size(); i < n; ++i) {
			final Flyout flyout = mDeletedFlyouts.get(i);
			mFlyouts.remove(flyout);
		}
		mDeletedFlyouts.clear();
	}

	public void onTrashDamage(Trash trash) {
		mHygieneBar.addHygiene(-trash.getData().getDecay());
	}

	private void debugSpawnTrash(long time) {
		// if there's not visitor, quit
		if (mVisitor.size() == 0) return;
		
		mTrashVariationEachSpawn += time / (float)2500.0f;
		mGameTime += time;
		if (mGameTime > mSpawnTime) {
			mGameTime -= mSpawnTime;
			// spawn!!

			int rand = Math.abs(GameRandom.intRandom((int)mTrashVariationEachSpawn));

			for (int i = 0; i < rand; ++i) {
				final int randomData = GameRandom.unsignedIntRandom(2);
				final int randomVisitorIndex = GameRandom.unsignedIntRandom(mVisitor.size());
				final Visitor visitor = mVisitor.get(randomVisitorIndex);
				final float randomX = visitor.position.x + GameRandom.intRandom(GameGlobals.GAME_TILE_WIDTH);
				final float randomY = visitor.position.y + GameRandom.intRandom(GameGlobals.GAME_TILE_WIDTH);
				Trash trash = new Trash(TrashFactory.instance().getData(randomData), GameRandom.intRandom(180));
				
				trash.throwTrash(visitor.position, new Vertex2D(randomX, randomY));
				trash.setTrashListener(this);
				
				mTrashes.add(trash);
			}
		}
	}

	private void debugSpawnVisitor(long time) {
		if (mGameTime < time) {
			// spawn!!
			final Visitor people = new Visitor(VisitorFactory.instance().getData(GameRandom.unsignedIntRandom(3)));
			people.setPath(mMap.getPath(GameRandom.unsignedIntRandom(mMap.getPathsCount())));
			mVisitor.add(people);
		}
	}

	public void draw(GL10 gl) {
		// draw all trashes, while keep checking the trash collection size
		for (int i = 0; i < mTrashes.size(); ++i) {
			final Trash trash = mTrashes.get(i);
			trash.draw(gl);
		}
		
		for (int i = 0; i < mVisitor.size(); ++i) {
			final Visitor flyout = mVisitor.get(i);
			flyout.draw(gl);
		}

		for (int i = 0; i < mFlyouts.size(); ++i) {
			final Flyout flyout = mFlyouts.get(i);
			flyout.draw(gl);
		}
		
		mTrashBin.draw(gl);
		
		mMap.draw(gl);
		
		// draw interface
		if (mHygieneBar != null) {
			mHygieneBar.draw(gl);
		}
	}
	
	public ArrayList<Trash> getTrashesOnPoint(float x, float y) {
		mCacheTrashes.clear();
		
		mCacheVertex2.x = x;
		mCacheVertex2.y = y;
		
		for (int i = 0, n = mTrashes.size(); i < n; ++i) {
			final Trash trash = mTrashes.get(i);
			
			mCacheVertex1.x = trash.position.x + trash.getWidth() / 2;
			mCacheVertex1.y = trash.position.y + trash.getHeight() / 2;

			if (Vertex3D.range(mCacheVertex1, mCacheVertex2) < GameGlobals.GAME_TILE_WIDTH / 1.25f) {
				mCacheTrashes.add(trash);
			}
		}

		return mCacheTrashes;
	}
	
	public MapRenderer getMapRenderer() {
		if (mMap != null) {
			return mMap.getRenderer();
		}
		return null;
	}

	public void onDestroy() {
		
	}
	
	public void onPause() {
		mPaused = true;
	}
	
	public void onResume() {
		mPaused = false;
	}

	public void setOnScoreChangedListener(OnScoreChangedListener listener) {
		mScoreChangedListener = listener;
	}
	
	public void setOnTimeChangedListener(OnTimeChangedListener listener) {
		mTimeChangedListener = listener;
	}
	
	public void setHygieneBar(HygieneBar bar) {
		mHygieneBar = bar;
		mHygieneBar.position.x = 0;
		mHygieneBar.position.y = -mHygieneBar.getHeight() * 1.5f;
	}

	public boolean isPaused()	{	return mPaused;	}
	
	public interface OnScoreChangedListener {
		public void onScoreChange(int currentScore);
	}
	
	public interface OnTimeChangedListener {
		public void onTimeChange(long currentTimeInMillis);
	}

	// debug mode
	private int mSpawnTime;
	private int mGameTime;
	private float mTrashVariationEachSpawn;
	
	// save the jsonString from map
	private String mMapJSONString;
	
	// entities
	private ArrayList<Trash> mTrashes;
	private ArrayList<Visitor> mVisitor;
	private ArrayList<Flyout> mFlyouts;
	private Map mMap;
	private Bin mTrashBin;
	private HygieneBar mHygieneBar;
	
	// deleted entities
	private ArrayList<Trash> mDeletedTrashes;
	private ArrayList<Visitor> mDeletedVisitor;
	private ArrayList<Flyout> mDeletedFlyouts;
	
	// cache entities
	private ArrayList<Trash> mCacheTrashes;
	private ArrayList<Visitor> mCacheVisitor;
	private ArrayList<Flyout> mCacheFlyouts;
	
	// cache vertex for computation
	private Vertex3D mCacheVertex1, mCacheVertex2;
	
	// scoring
	private int mScore;
	
	// game listener
	private OnScoreChangedListener mScoreChangedListener;
	private OnTimeChangedListener mTimeChangedListener;
	
	private long mGameDuration;
	private boolean mRunning, mPaused, mEntitiesReady;
}
