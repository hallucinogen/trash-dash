package net.gogo.mobile.td.gl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import net.gogo.mobile.framework.glwrapper.GLTexture;
import net.gogo.mobile.td.game.GameGlobals;
import net.gogo.mobile.td.game.level.Tileset;

import android.util.Log;

public class MapRenderer {
	private static final String TAG = "Map Renderer";
	public MapRenderer(Tileset tileset, int[][] tiles) {
		mTileset		= tileset;
		mTiles			= tiles;
		mVBOPrepared 	= false;
		mMainTexture	= tileset.getTile(tiles[0][0]).texture;
	}
	
	private void prepareVBO(GL11 gl) {
		// prepare array
		mQuadH = mTiles[0].length;
		mQuadW = mTiles.length;
		final int quadCount	= mQuadW * mQuadH;
		
		final float depth 	= -1.02f;
		
		mVertexBuffer = ByteBuffer.allocateDirect(quadCount * 4 * 4 * 3)
    	.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTextureBuffer = ByteBuffer.allocateDirect(quadCount * 4 * 4 * 2)
    	.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mIndexBuffer = ByteBuffer.allocateDirect(quadCount * 6 * 2)
		.order(ByteOrder.nativeOrder()).asShortBuffer();
		
		final float[] vertex 	= new float[12 * quadCount];
		final float[] texture 	= new float[8 * quadCount];
		final short[] index		= new short[6 * quadCount];
		
		int textureIndex = 0;
		int vertexIndex = 0;
		int	indexIndex = 0;
		
		for (int y = 0; y < mQuadH; ++y) {
			for (int x = 0; x < mQuadW; ++x) {
				final float left	= (x 			* GameGlobals.GAME_TILE_WIDTH	/*- GameGlobals.GAME_SCREEN_WIDTH / 2 */);
				final float right	= ((x + 1) 		* GameGlobals.GAME_TILE_WIDTH  	/*- GameGlobals.GAME_SCREEN_WIDTH / 2*/);
				final float top		= (-((y + 1) 	* GameGlobals.GAME_TILE_HEIGHT)	/*+ GameGlobals.GAME_SCREEN_WIDTH / 2*/);
				final float bottom	= (-(y 			* GameGlobals.GAME_TILE_HEIGHT)	/*+ GameGlobals.GAME_SCREEN_WIDTH / 2*/);

				vertex[vertexIndex++] = left;
				vertex[vertexIndex++] = top;
				vertex[vertexIndex++] = depth;
				
				vertex[vertexIndex++] = right;
				vertex[vertexIndex++] = top;
				vertex[vertexIndex++] = depth;
				
				vertex[vertexIndex++] = left;
				vertex[vertexIndex++] = bottom;
				vertex[vertexIndex++] = depth;
				
				vertex[vertexIndex++] = right;
				vertex[vertexIndex++] = bottom;
				vertex[vertexIndex++] = depth;
				
				for (int c = 0, n = mTileset.getTile(mTiles[y][x]).crop.length; c < n; ++c) {
					texture[textureIndex++] = mTileset.getTile(mTiles[y][x]).crop[c];
				}

				final short nox = (short)((y * mQuadW + x) * 4);
				index[indexIndex++] = nox;
				index[indexIndex++] = (short)(nox + 1);
				index[indexIndex++] = (short)(nox + 2);
				index[indexIndex++] = (short)(nox + 1);
				index[indexIndex++] = (short)(nox + 2);
				index[indexIndex++] = (short)(nox + 3);
			}
		}
		
		mVertexBuffer.put(vertex);
		mTextureBuffer.put(texture);
		mIndexBuffer.put(index);
		
		mVertexBuffer.position(0);
		mTextureBuffer.position(0);
		mIndexBuffer.position(0);
		
		// prepare VBO
		int[] buffer = new int[1];
		
		gl.glGenBuffers(1, buffer, 0);
		mVertexPointer = buffer[0];
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, mVertexPointer);
        gl.glBufferData(GL11.GL_ARRAY_BUFFER, quadCount * 4 * 3 * 4, mVertexBuffer, GL11.GL_STATIC_DRAW);
        
        gl.glGenBuffers(1, buffer, 0);
		mTexturePointer = buffer[0];
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, mTexturePointer);
        gl.glBufferData(GL11.GL_ARRAY_BUFFER, quadCount * 4 * 2 * 4, mTextureBuffer, GL11.GL_STATIC_DRAW);
		
		gl.glGenBuffers(1, buffer, 0);
		mIndexPointer = buffer[0];
		gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, mIndexPointer);
        gl.glBufferData(GL11.GL_ELEMENT_ARRAY_BUFFER, quadCount * 6 * 2, mIndexBuffer, GL11.GL_STATIC_DRAW);
        
        // unbind buffers
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);
        gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		mVBOPrepared = true;
	}
	
	public void draw(GL11 gl) {
		if (!mVBOPrepared)
			prepareVBO(gl);
		
		final int quadCount = mQuadH * mQuadW;
		
		// prepare draw
		//gl.glMatrixMode(GL10.GL_MODELVIEW);
		//gl.glPushMatrix();

		// draw map
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mMainTexture.name);
		gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, mVertexPointer);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, 0);
        gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, mTexturePointer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, 0);
        gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, mIndexPointer);
        gl.glDrawElements(GL11.GL_TRIANGLES, 6 * quadCount, GL11.GL_UNSIGNED_SHORT, 0);
		
        // unbind buffers
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);
        gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, 0);
        
        //gl.glPopMatrix();
	}
	
	public void invalidateVBO() {
		mVBOPrepared = false;
	}
	
	// cache vbo
	private boolean mVBOPrepared;
	
	// map stuffs
	private int mQuadW, mQuadH;
	private Tileset mTileset;
	private GLTexture mMainTexture;
	private int[][] mTiles;
	
	// VBO pointer
	private int mVertexPointer, mTexturePointer, mIndexPointer;
	
	// buffer for VBO preparation
	private FloatBuffer mVertexBuffer, mTextureBuffer;
	private ShortBuffer mIndexBuffer;
}
