package net.gogo.mobile.td.gl;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import android.content.Context;
import android.util.Log;

import net.gogo.mobile.framework.glwrapper.BasicRenderer;
import net.gogo.mobile.framework.utilities.DrawUtilities;
import net.gogo.mobile.td.game.Game;
import net.gogo.mobile.td.game.GameGlobals;
import net.gogo.mobile.td.game.model.FlyoutFactory;
import net.gogo.mobile.td.game.model.HygieneBar;
import net.gogo.mobile.td.game.model.VisitorFactory;
import net.gogo.mobile.td.game.model.TrashFactory;

public class GameRenderer extends BasicRenderer {
	private final static String TAG = "Game Renderer";

	public GameRenderer(Context context) {
		super(context);
		mTextureValidated = false;

		// disable debug
		mFPSDebugEnabled = false;
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		super.onDrawFrame(gl);

		// when texture is invalidated (because of game paused), reload them
		if (!mTextureValidated) {
			GLSystem.GLTextureLibrary.loadAll(mContext, gl);

			mTextureValidated = true;
		}

		// clear the screen
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

		// move the camera to adjust coordinate to 0,0
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();

		gl.glTranslatef(-GameGlobals.GAME_SCREEN_WIDTH / 2,
				GameGlobals.GAME_SCREEN_WIDTH / 2, 0);

		if (mGame != null) {
			mGame.draw(gl);
		} else {
			Log.e(TAG, "Game is null when the renderer trying to draw!");
		}
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		super.onSurfaceChanged(gl, width, height);

		// save the scaling, this scaling is needed if we want to adjust with
		// all resolution
		GameGlobals.GAME_SCREEN_WIDTH = width;
		GameGlobals.GAME_SCREEN_HEIGHT = height;

		// tile size = min of screen width and height divide by 10 (map consist
		// of 10 tiles)
		GameGlobals.GAME_TILE_WIDTH = Math.min(width, height) / 10;
		GameGlobals.GAME_TILE_HEIGHT = GameGlobals.GAME_TILE_WIDTH;

		// reload all data to let all factories resize sprite data
		TrashFactory.instance(mContext);
		VisitorFactory.instance(mContext);
		FlyoutFactory.instance(mContext);
        mGame.setHygieneBar(new HygieneBar(mContext));
		// hey ho! let's resize the entities of the game fitting the screen
		// resolution!
		mGame.resizeEntities();
	}

	public void setGame(Game game) {
		mGame = game;
	}

	public void onDestroy() {

	}

	public void onResume() {
		GLSystem.GLTextureLibrary.invalidateAll();
		mTextureValidated = false;
		if (mGame != null && mGame.getMapRenderer() != null) {
			mGame.getMapRenderer().invalidateVBO();
		}

		DrawUtilities.instance().flush((GL11) mGL);
	}

	public void onPause() {

	}

	private Game mGame;

	private boolean mTextureValidated;
}
