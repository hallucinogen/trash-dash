package net.gogo.mobile.td.gl;

import android.content.Context;
import net.gogo.mobile.framework.glwrapper.GLFontLibrary;
import net.gogo.mobile.framework.glwrapper.GLTextureLibrary;

public class GLSystem {
	public static GameRenderer GameRenderer;
	public static GLTextureLibrary GLTextureLibrary;
	public static GLFontLibrary GLFontLibrary;
	
	public static void instantiateGL(Context context) {
		GameRenderer = new GameRenderer(context);
		GLTextureLibrary = new GLTextureLibrary();
		GLFontLibrary = new GLFontLibrary();
	}
}
