package net.gogo.mobile.td;
import java.io.IOException;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

import net.gogo.mobile.framework.utilities.FileUtilities;
import net.gogo.mobile.framework.utilities.SoundManager;
import net.gogo.mobile.td.game.Game;
import net.gogo.mobile.td.game.Game.OnTimeChangedListener;
import net.gogo.mobile.td.game.GameTouchListener;
import net.gogo.mobile.td.game.model.HygieneBar;
import net.gogo.mobile.td.game.model.TrashFactory;
import net.gogo.mobile.td.gl.GLSystem;
import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class GameAct extends Activity implements Game.OnScoreChangedListener, OnTimeChangedListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        
        // instantiate gl system
        GLSystem.instantiateGL(this);

        // TODO: map must go from outside
        String mapJSON = null;
        try {
        	mapJSON = FileUtilities.readFileAsString(getAssets().open("data/map/map1.json"));
        } catch (IOException iEx) {
        }

        // instantiate game
        mGame = new Game(mapJSON);
        mTouchListener = new GameTouchListener(mGame);
        GLSystem.GameRenderer.setGame(mGame);
        
        // instantiate view
        mScore 	= (TextView) findViewById(R.id.score);
        mTime 	= (TextView) findViewById(R.id.time);

        // instantiate surface
        GLSurfaceView surface = (GLSurfaceView)findViewById(R.id.game_screen);
        surface.setRenderer(GLSystem.GameRenderer);

        // set surface listener
        surface.setOnTouchListener(new View.OnTouchListener() {
        	public boolean onTouch(View v, MotionEvent event) {
        		try {
        			mTouchListener.onTouch(event);
        			// sleep the thread so it won't be flooded with event
        			Thread.sleep(16);
        		} catch (InterruptedException inEx) {
        			
        		}
				return true;
			}
		});

        // start the game!
        mGame.start();
        mGame.setOnScoreChangedListener(this);
        mGame.setOnTimeChangedListener(this);

        // make the ad view
		mAdView = (AdView)findViewById(R.id.ad);
		mAdView.loadAd(new AdRequest());
    }

    @Override
    protected void onDestroy() {
    	mAdView.destroy();
    	SoundManager.instance(this).stopAllSound();
    	super.onDestroy();
    }
    
    @Override
    protected void onResume() {
    	GLSystem.GameRenderer.onResume();
    	mGame.onResume();
    	
    	// play the music!!
    	SoundManager.instance(this).playBGM(R.raw.garage, true);
    	super.onResume();
    }
    
    @Override
    protected void onPause() {
    	// pause the renderer
    	GLSystem.GameRenderer.onPause();
    	mGame.onPause();
    	super.onPause();
    }
    
    public void onScoreChange(int currentScore) {
    	Message message = new Message();
    	message.obj		= currentScore;
    	message.what 	= SCORE_CHANGE_MESSAGE;
    	mGameHandler.sendMessage(message);
    	
    	// play sound
    	SoundManager.instance(this).playSFX(R.raw.get_score);
    }
    
    public void onTimeChange(long currentTimeInMillis) {
    	Message message = new Message();
    	// turn to second
    	message.obj 	= currentTimeInMillis / 1000;
    	message.what 	= TIME_CHANGE_MESSAGE;
    	mGameHandler.sendMessage(message);
    }

    private Handler mGameHandler = new Handler() {
    	@Override
    	public void handleMessage(Message msg) {
    		switch (msg.what) {
    		case SCORE_CHANGE_MESSAGE:
        		mScore.setText(((Integer)msg.obj).toString());
    			break;
    		case TIME_CHANGE_MESSAGE:
    			mTime.setText(((Long)msg.obj).toString());
    			break;
    		}
    	}
    };
    
    private final static int SCORE_CHANGE_MESSAGE 	= 0;
    private final static int TIME_CHANGE_MESSAGE 	= 1;
    
    private TextView mScore, mTime;
    
    private Game mGame;
    private AdView mAdView;
    private GameTouchListener mTouchListener;
}